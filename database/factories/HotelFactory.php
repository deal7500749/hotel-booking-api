<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Hotel;

class HotelFactory extends Factory
{
    protected $model = Hotel::class;

    public function definition()
    {
        return [
            'id' => $this->faker->randomDigitNotNull(),
            'name' => $this->faker->name(),
            'street' => $this->faker->streetAddress(),
            'postcode' => $this->faker->postcode(),
            'place' => $this->faker->city(),
            'description' => $this->faker->text()
        ];
    }
}