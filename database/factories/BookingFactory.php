<?php

namespace Database\Factories;

use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingFactory extends Factory
{
    protected $model = Booking::class;

    public function definition()
    {
        return [
            'id' => $this->faker->randomDigitNotNull(),
            'booking_number' => 'BN0001',
            'customer_id' => function() {
                return \App\Models\Customer::factory()->create()->id;
            },
            'room_id' => function () {
                return \App\Models\Room::factory()->create()->id;
            },
            'check_in_date' => Carbon::parse('22-02-2023'),
            'check_out_date' => Carbon::parse('26-02-2023'),
            'total_amount' => $this->faker->randomFloat(2, 50, 500),
        ];
    }
}