<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Room;

class RoomFactory extends Factory
{
    protected $model = Room::class;

    public function definition()
    {
        return [
            'id' => $this->faker->randomDigitNotNull(),
            'hotel_id' => \App\Models\Hotel::factory()->create()->id,
            'room_number' => $this->faker->randomNumber(),
            'type' => 'suite',
            'price_per_night' => 120.00,
            'status' => 'available'
        ];
    }
}