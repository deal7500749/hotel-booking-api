<?php

namespace Database\Seeders;

use App\Models\Hotel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HotelSeeder extends Seeder
{
    /**
    * Run the database seeds.
    */
    public function run()
    {
        $this->createHotel('Luxury Resort & Spa', '123 Beachfront Avenue', '1234 AB', 'Amsterdam', 'A luxurious resort overlooking the ocean.');
        $this->createHotel('Cityscape Hotel', '789 Downtown Street', '5678 CD', 'Rotterdam', 'Conveniently located in the heart of the city.');
        $this->createHotel('Mountain Lodge Retreat', '456 Forest Trail', '9012 EF', 'Utrecht', 'Escape to the tranquility of the mountains.');
        $this->createHotel('Riverside Inn', '101 Riverwalk Lane', '3456 GH', 'The Hague', 'Experience scenic views by the river.');
        $this->createHotel('Historic Manor House', '555 Heritage Avenue', '7890 IJ', 'Eindhoven', 'Step back in time at this historic estate.');
    }

    private function createHotel($name, $street, $postcode, $place, $description)
    {
        Hotel::create([
            'name' => $name,
            'street' => $street,
            'postcode' => $postcode,
            'place' => $place,
            'description' => $description,
        ]);
    }
}
