<?php

namespace App\Services;

use App\Mail\BookingConfirmation;
use App\Models\Booking;
use App\Models\Customer;
use Illuminate\Support\Facades\Mail;

class BookingService 
{
    public function calculateTotalBookingPrice($checkInDate, $checkOutDate, $pricePerNight)
    {
        return $checkOutDate->diffInDays($checkInDate) * $pricePerNight;
    }

    public function generateBookingNumber()
    {
        $lastBooking = Booking::latest()->first();
        $sequentialNumber = $lastBooking ? (int)substr($lastBooking->booking_number, 2) + 1 : 1;
        $formattedSequentialNumber = str_pad($sequentialNumber, 4, '0', STR_PAD_LEFT);
        $bookingNumber = 'BN' . $formattedSequentialNumber;

        return $bookingNumber;
    }

    public function generateBookingConfirmationUrl($bookingId)
    {
        return 'http://localhost/api/booking/' . $bookingId;
    }

    public function generateConfirmationMailData($customerId, $bookingId): void
    {
        $customer = Customer::findOrFail($customerId);
        $customerMail = $customer->email;

        $bookingConfirmationUrl = $this->generateBookingConfirmationUrl($bookingId);
        $booking = Booking::findOrFail($bookingId);
        $bookingNumber = $booking->booking_number;

        $mailData = [
            'title' => 'Booking confirmation',
            'subject' => 'Booking confirmation ' . $bookingNumber,
            'booking_number' => $bookingNumber,
            'body' => 'Here is your booking confirmation',
            'url' => $bookingConfirmationUrl
        ];

       $this->sendBookingConfirmationMail($customerMail, $mailData);
    }

    public function sendBookingConfirmationMail($customerMail, $mailData)
    {
        Mail::to($customerMail)->send(new BookingConfirmation($mailData));
    }
}