<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\Room;
use App\Services\BookingService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    private $bookingService;

    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    public function store(Request $request)
    {
        $request->validate([
            'room_id' => 'required|exists:rooms,id',
            'check_in_date' => 'required|date',
            'check_out_date' => 'required|date|after:check_in_date',
        ]);

        $customerId = auth()->user()->id;

        $roomId = $request->room_id;
        $room = Room::findOrFail($roomId);

        if(!$room->isAvailable()) {
            return response()->json([
                'message' => 'Room is not available'
            ], 409);
        }

        $pricePerNight = $room->price_per_night;

        $checkInDate = Carbon::parse($request->check_in_date);
        $checkOutDate = Carbon::parse($request->check_out_date);

        $totalPrice = $this->bookingService->calculateTotalBookingPrice($checkInDate, $checkOutDate, $pricePerNight);
        $bookingNumber = $this->bookingService->generateBookingNumber();

        $booking = Booking::create([
            'booking_number' => $bookingNumber,
            'customer_id' => $customerId,
            'room_id' => $roomId,
            'check_in_date' => $checkInDate,
            'check_out_date' => $checkOutDate,
            'total_amount' => $totalPrice,
        ]);

        if (!$booking) {
            return response()->json(['message' => 'Failed to create booking'], 500);
        }

        $this->bookingService->generateConfirmationMailData($customerId, $booking->id);

        return response()->json([
            'message' => 'Booking created successfully', 
            'booking' => $booking
        ], 201);
    }

    public function show($id)
    {
        $booking = Booking::findOrFail($id);
        $customer = Customer::findOrFail($booking->customer_id);

        return response()->json([
            'message' => 'successfully found booking confirmation',
            'booking' => $booking,
            'customer' => $customer
        ], 200);
    }
}