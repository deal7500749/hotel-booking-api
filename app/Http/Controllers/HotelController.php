<?php

namespace App\Http\Controllers;

use App\Models\Hotel;

class HotelController extends Controller
{    
    public function show($id)
    {
        $hotel = Hotel::findOrFail($id);

        return response()->json([
            'message' => 'successfully found hotel',
            'hotel' => $hotel
        ], 200);
    }
}
