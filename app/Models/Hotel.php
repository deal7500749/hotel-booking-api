<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'street', 'postcode', 'place', 'description'];

    public function rooms() {
        return $this->hasMany(Room::class);
    }
}
