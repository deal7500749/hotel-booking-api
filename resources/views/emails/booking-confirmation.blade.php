<!DOCTYPE html>
<html>
<head>
    <title>{{ $mailData['title'] }}</title>
</head>
<body>
    <h1>{{ $mailData['title'] }}</h1>
    <p>Booking Number: {{ $mailData['booking_number'] }}</p>
    <p>{{ $mailData['body'] }}</p>
    <a href="{{ $mailData['url'] }}">View confirmation</a>
</body>
</html>
