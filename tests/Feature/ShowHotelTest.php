<?php

namespace Tests\Feature;

use App\Models\Hotel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowHotelTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $hotel = Hotel::factory()->create();

        $response = $this->get('http://localhost/api/hotel/' . $hotel->id);
        $response->assertStatus(200);
    }
}
