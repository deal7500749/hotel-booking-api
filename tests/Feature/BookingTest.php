<?php

namespace Tests\Feature;

use App\Models\Booking;
use App\Models\Customer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookingTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function testIfBookingDataIsValid(): void
    {
        $booking = Booking::factory()->create()->toArray();

        // $response = $this->get('http://localhost/api/booking/' . $booking->id);
        // $response->assertStatus(200);

        $registerCustomerResponse =  $this->postJson('api/customer/register', $this->createCustomerData());

        $apiToken = $registerCustomerResponse->json()['token'];

        $headers = [
            'Authorization' => 'Bearer ' . $apiToken,
            'Accept' => 'application/json',
        ];

        $bookingResponse = $this->postJson('/api/rooms/book', $booking, $headers);

        $bookingResponse->assertStatus(201);
    }

    private function createCustomerData()
    {
        return [
            'name' => 'rob geus',
            'email' => 'rob.g@hotmail.com',
            'phone' => '0681485236',
            'password' => 'hallo1234!'
        ];
    }
}
