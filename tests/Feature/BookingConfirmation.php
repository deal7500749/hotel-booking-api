<?php

namespace Tests\Feature;

use App\Models\Booking;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookingConfirmationTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $booking = Booking::factory()->create();

        $response = $this->get('http://localhost/api/booking/' . $booking->id);
        $response->assertStatus(200);
    }
}
