<?php

use App\Http\Controllers\BookingController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\HotelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::middleware('auth:sanctum')->group(function () {
    Route::post('/rooms/book', [BookingController::class, 'store']);
});

Route::post('customer/register', [CustomerController::class, 'register']);

Route::get('/booking/{id}', [BookingController::class, 'show']);

Route::get('/hotel/{id}', [HotelController::class, 'show']);